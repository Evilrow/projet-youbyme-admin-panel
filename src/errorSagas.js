import {CRUD_CREATE_FAILURE, CRUD_UPDATE_FAILURE} from "react-admin"; 
import {stopSubmit} from 'redux-form'; 
import {put, takeEvery} from "redux-saga/effects";

export default function* errorSagas() {
    yield takeEvery([CRUD_CREATE_FAILURE, CRUD_UPDATE_FAILURE], crudCreateFailure);
}

export function* crudCreateFailure(action) {
    var json = action.payload; 
    // The extractViolations function searches in the response's json 
    // all the violation and output a json structure looking like this: 
    // { // age: "You must be at least 13 years old to use this service", 
    // username: "This username is already taken" 
    // } 
    //const violations = extractViolations(json); 
    yield put(stopSubmit('record-form', json)); 
}