import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

export default () => (
    <Card>
        <CardHeader title="Bienvenue sur la page d'aministration de l'application YouByMe" />
        <CardContent>CRUD de tout !</CardContent>
    </Card>
);