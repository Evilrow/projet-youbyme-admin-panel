import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR } from 'react-admin';

export default (type, params) => {
    if (type === AUTH_LOGIN) {

        const { username, password } = params;
        console.log(username);
        console.log(password);
        const request = new Request('http://192.168.42.221/auth/login/', {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })

        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ key }) => {
                localStorage.setItem('key', key);
            });
    }
    if (type === AUTH_LOGOUT) {
        const request = new Request('http://192.168.42.221/auth/logout/', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })
        return fetch(request)
        .then(({ key }) => {
            localStorage.removeItem('key');
        });
    }
    if (type === AUTH_ERROR) {
        const status  = params.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('key');
            return Promise.reject();
        }
        return Promise.resolve();
    }
    return Promise.resolve();
}