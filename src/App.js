import React from 'react';
import dataProvider from './dataProvider';
import {Admin, Resource} from 'react-admin';
import frenchMessages from 'ra-language-french';
import {UserList, UserEdit, UserCreate} from './entities/users';
import {CategoryList, CategoryEdit, CategoryCreate} from './entities/categorys';
import {SkillList, SkillEdit, SkillCreate} from './entities/skills';
import {AreaList, AreaEdit, AreaCreate} from './entities/areas';
import {SectorList, SectorEdit, SectorCreate} from './entities/sectors';
import {GroupList, GroupEdit, GroupCreate} from './entities/groups';
import {PromoList, PromoEdit, PromoCreate} from './entities/promos';
import {UserskillList, UserskillEdit, UserskillCreate} from './entities/userskills';
import {LogList} from './entities/logs';
import Dashboard from './dashboard'
import UserIcon from '@material-ui/icons/Group';
import authProvider from './authProvider';
import NotFound from './NotFound';
import errorSagas from './errorSagas';
import CustomLayout from './customComponents/CustomLayout';
import CustomRoutes from './customComponents/CustomRoutes';
import { RoutesWithLayout } from 'ra-core';

const messages = {
  fr: frenchMessages
}
const i18nProvider = locale => messages[locale];

const App = () => (
  <Admin
      appLayout={CustomLayout}
      customSagas={[ errorSagas ]}
      locale="fr"
      i18nProvider={i18nProvider}
      catchAll={NotFound} 
      dashboard={Dashboard} 
      authProvider={authProvider} 
      dataProvider={dataProvider}
      customRoutes={CustomRoutes}
  >
      <Resource name="users" icon={UserIcon} list={UserList} create={UserCreate}/>
      <Resource name="groups" list={GroupList} edit={GroupEdit} create={GroupCreate} />
      <Resource name="userskills" list={UserskillList} edit={UserskillEdit} create={UserskillCreate} />
      <Resource name="categories" list={CategoryList} edit={CategoryEdit} create={CategoryCreate} />
      <Resource name="skills" list={SkillList} edit={SkillEdit} create={SkillCreate} />
      <Resource name="sectors" list={SectorList} edit={SectorEdit} create={SectorCreate} />
      <Resource name="promo" list={PromoList} edit={PromoEdit} create={PromoCreate} />
      <Resource name="areas" list={AreaList} edit={AreaEdit} create={AreaCreate} />
      <Resource name="logs" list={LogList} />
      
  </Admin>
);

export default App;