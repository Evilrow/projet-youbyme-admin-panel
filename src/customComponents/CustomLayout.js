import React from 'react';
import { AppBar, UserMenu, MenuItemLink , Layout} from 'react-admin';
import SettingsIcon from '@material-ui/icons/Settings';

const MyUserMenu = props => (
    <UserMenu {...props}>
        <MenuItemLink
            to="/configuration"
            primaryText="Configuration"
            leftIcon={<SettingsIcon />}
        />
    </UserMenu>
);

const MyAppBar = props => <AppBar {...props} userMenu={<MyUserMenu />} />;

const CustomLayout = props => <Layout {...props} appBar={MyAppBar} />;

export default CustomLayout;