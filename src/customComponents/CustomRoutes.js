import React from 'react';
import { Route } from 'react-router-dom';
import ConfigurationPanel from '../ConfigurationPanel';

export default [
    <Route exact path="/Configuration" component={ConfigurationPanel} />,
];