import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    DELETE,
    fetchUtils,
} from 'react-admin';
import { stringify } from 'query-string';

const API_URL = 'http://192.168.42.221';

/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */

const convertDataProviderRequestToHTTP = (type, resource, params) => {
    //-- Header par défaut
    var headerDefault = new Headers({'Accept': '*/*', 'Authorization': 'Token ' + localStorage.getItem('key')});
    switch (type) {
        //-- Récupère liste
        case GET_LIST: {
            const { field, order } = params.sort;
            const query = { 'field': field, 'order': order }
            if (query.order === "DESC") {
                query.field = '-' + field
            }
            return {
                url: `${API_URL}/${resource}/?ordering=${query.field}&page=${params.pagination.page}&format=json`,
                options: {
                    headers: headerDefault,
                    method: 'GET'
                }
            };
        }
        //-- Récupère 1
        case GET_ONE:
            return { url: `${API_URL}/${resource}/${params.id}/`, 
                     options: {headers: headerDefault},
                };
        case GET_MANY: {
            return { url: `${API_URL}/${resource}/`,
                    options: {headers: headerDefault}, 
                };
        }
        case GET_MANY_REFERENCE: {
            const { page, perPage } = params.pagination;
            const { field, order } = params.sort;
            const query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([(page - 1) * perPage, (page * perPage) - 1]),
                filter: JSON.stringify({ ...params.filter, [params.target]: params.id }),
            };
            return { url: `${API_URL}/${resource}/?${stringify(query)}`,
                     options: {headers: headerDefault}
            };
        }
        case UPDATE:
            return {
                url: `${API_URL}/${resource}/${params.id}/`,
                options: {headers: headerDefault,  method: 'PUT', body: JSON.stringify(params.data), },
            };
        case CREATE:
        if(resource =='users')
            resource = 'auth/registration';

            return {
                url: `${API_URL}/${resource}/`,
                options: {headers: headerDefault, method: 'POST', body: JSON.stringify(params.data),},
            };
        case DELETE:
            return {
                url: `${API_URL}/${resource}/${params.id}/`,
                options: {headers: headerDefault, method: 'DELETE' },
            };
        default:
            throw new Error(`Unsupported fetch action type ${type}`);
    }
};

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} Data Provider response
 */
const convertHTTPResponseToDataProvider = (response, type, resource, params) => {
    const { json } = response;
    switch (type) {
        case GET_LIST:

            for (var i in json.results) {
                for (var key in json.results[i]) {
                    if (typeof (json.results[i][key]) == 'string') {
                        if (json.results[i][key].includes("http") && !json.results[i][key].includes(resource)) {
                            var split = json.results[i][key].split('/');
                            split = split[split.length - 2];
                            json.results[i][key] = split;
                        }
                    }
                }
            }
            return {
                data: json.results,
                total: json.count,
            };
        case GET_MANY:
            return {
                data: json.results,
                total: json.count,
            };
        case CREATE:
            return { data: { ...params.data, id: json.id } };
        default:
            return { data: json };
    }
};

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for response
 */
export default (type, resource, params) => {
    const { fetchJson } = fetchUtils;
    var { url, options } = convertDataProviderRequestToHTTP(type, resource, params);

    //-- Envoie requete avec url et option (header & method)
    return fetchJson(url, options)
        .then(response => convertHTTPResponseToDataProvider(response, type, resource, params));
};