import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';


const GroupTitle = ({ record }) => {
    return <span>Group {record ? `${record.lib}` : ''}</span>;
};

export const GroupList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const GroupEdit = props => (
    <Edit undoable={false} title={<GroupTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="name" />
        </SimpleForm>
    </Edit>
);

export const GroupCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="name" />
        </SimpleForm>
    </Create>
);