import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create, ReferenceField, ReferenceInput, SelectInput } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

const UserskillTitle = ({ record }) => {
    return <span>Skill {record ? `${record.lib}` : ''}</span>;
};

export const UserskillList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="points_number" />
            <ReferenceField source="skill" reference="skills" allowEmpty>
                <TextField source="lib" />
            </ReferenceField>
            <ReferenceField source="user" reference="users" allowEmpty>
                <TextField source="username" />
            </ReferenceField>
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const UserskillEdit = props => (
    <Edit undoable={false} title={<UserskillTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="points_number" />
            <ReferenceInput source="skill" reference="skills" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
            <ReferenceInput source="user" reference="users" allowEmpty>
                <SelectInput optionText="username" optionValue="url"/>
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export const UserskillCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list"> 
            <TextInput source="points_number" />
            <ReferenceInput source="skill" reference="skills" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
            <ReferenceInput source="user" reference="users" allowEmpty>
                <SelectInput optionText="username" optionValue="url"/>
            </ReferenceInput>
        </SimpleForm>
    </Create>
);