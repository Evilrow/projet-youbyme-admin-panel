import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create, ReferenceField, ReferenceInput, SelectInput } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

const PromoTitle = ({ record }) => {
    return <span>Area {record ? `${record.lib}` : ''}</span>;
};

export const PromoList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="lib" />
            <ReferenceField source="sector" reference="sectors" allowEmpty>
                <TextField source="lib" />
            </ReferenceField>
            <ReferenceField source="area" reference="areas" allowEmpty>
                <TextField source="lib" />
            </ReferenceField>
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const PromoEdit = props => (
    <Edit undoable={false} title={<PromoTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="lib" />
            <ReferenceInput source="sector" reference="sectors" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
            <ReferenceInput source="area" reference="areas" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export const PromoCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="lib" />
            <ReferenceInput source="sector" reference="sectors" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
            <ReferenceInput source="area" reference="areas" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
        </SimpleForm>
    </Create>
);