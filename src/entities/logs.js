import React from 'react';
import { List, Datagrid, TextField, BooleanField, ReferenceField } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

export const LogList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <ReferenceField source="from_user" reference="users" allowEmpty>
                <TextField source="username" />
            </ReferenceField>
            <ReferenceField source="to_user" reference="users" allowEmpty>
                <TextField source="username" />
            </ReferenceField>
            <TextField source="points_number_given" />
            <BooleanField source="is_bonus_point_given" />
            <BooleanField source="validation" />
        </Datagrid>
    </List>
);