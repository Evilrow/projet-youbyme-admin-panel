import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

const AreaTitle = ({ record }) => {
    return <span>Area {record ? `${record.lib}` : ''}</span>;
};

export const AreaList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="lib" />
            <TextField source="cp" />
            <TextField source="siret" />
            <TextField source="tel" />
            <TextField source="adr" />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const AreaEdit = props => (
    <Edit undoable={false} title={<AreaTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="lib" />
            <TextInput source="cp" />
            <TextInput source="siret" />
            <TextInput source="tel" />
            <TextInput source="adr" />
        </SimpleForm>
    </Edit>
);

export const AreaCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="lib" />
            <TextInput source="cp" />
            <TextInput source="siret" />
            <TextInput source="tel" />
            <TextInput source="adr" />
        </SimpleForm>
    </Create>
);