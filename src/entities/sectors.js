import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

const SectorTitle = ({ record }) => {
    return <span>Sector {record ? `${record.lib}` : ''}</span>;
};

export const SectorList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="lib" />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const SectorEdit = props => (
    <Edit undoable={false} title={<SectorTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="lib" />
        </SimpleForm>
    </Edit>
);

export const SectorCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="lib" />
        </SimpleForm>
    </Create>
);