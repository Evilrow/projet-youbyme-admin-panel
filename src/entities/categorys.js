import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

const CategoryTitle = ({ record }) => {
    return <span>Category {record ? `${record.lib}` : ''}</span>;
};

export const CategoryList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="lib" />
            <TextField source="description" />
            {/* <ImageField source="path_img"/> */}
            <TextField source="path_img" />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const CategoryEdit = props => (
    <Edit title={<CategoryTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="lib" />
            <TextInput source="description" />
            {/* <ImageInput source="path_img" accept="image/*" placeholder={<p>Déposez votre image ici</p>}>
                <ImageField source="path_img"/>
            </ImageInput> */}
            <TextInput source="path_img" />
        </SimpleForm>
    </Edit>
);

export const CategoryCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="lib" />
            <TextInput source="description" />
            {/* <ImageInput source="path_img" accept="image/*" placeholder={<p>Déposez votre image ici</p>}>
                <ImageField source="path_img"/>
            </ImageInput> */}
            <TextInput source="path_img" />
        </SimpleForm>
    </Create>
);