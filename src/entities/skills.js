import React from 'react';
import { List, Datagrid, TextField, Edit, EditButton, DeleteButton, SimpleForm, TextInput, DisabledInput, Create, ReferenceField, ReferenceInput, SelectInput } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

const SkillTitle = ({ record }) => {
    return <span>Skill {record ? `${record.lib}` : ''}</span>;
};

export const SkillList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="lib" />
            <TextField source="description" />
            {/* <ImageField source="path_img"/> */}
            <TextField source="path_img" />
            <ReferenceField source="category" reference="categories" allowEmpty>
                <TextField source="lib" />
            </ReferenceField>
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

export const SkillEdit = props => (
    <Edit undoable={false} title={<SkillTitle/>} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="lib" />
            <TextInput source="description" />
            {/* <ImageInput source="path_img" accept="image/*" placeholder={<p>Déposez votre image ici</p>}>
                <ImageField source="path_img"/>
            </ImageInput> */}
            <TextInput source="path_img" />
            <ReferenceInput source="category" reference="categories" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export const SkillCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="lib" />
            <TextInput source="description" />
            {/* <ImageInput source="path_img" accept="image/*" placeholder={<p>Déposez votre image ici</p>}>
                <ImageField source="path_img"/>
            </ImageInput> */}
            <TextInput source="path_img" />
            <ReferenceInput source="category" reference="categories" allowEmpty>
                <SelectInput optionText="lib" optionValue="url"/>
            </ReferenceInput>
        </SimpleForm>
    </Create>
);