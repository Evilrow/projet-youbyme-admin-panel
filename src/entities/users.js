import React from 'react';
import { List, Datagrid, TextField, EmailField, ReferenceField, Edit, EditButton, DeleteButton, DisabledInput, TextInput, SimpleForm, Create } from 'react-admin';
import CustomPagination from '../customComponents/CustomPagination';

export const UserList = props => (
    <List perPage={20} pagination={<CustomPagination />} bulkActionButtons={false} {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="username" />
            <EmailField source="email" />
            <ReferenceField source="groups" reference="groups" allowEmpty>
                <TextField source="name" />
            </ReferenceField>
            <ReferenceField source="user_skills" reference="userskills" allowEmpty>
                <TextField source="lib" />
            </ReferenceField>
        </Datagrid>
    </List>
);

export const UserCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
        <DisabledInput source="id" />
            <TextInput source="username" />
            <TextInput source="email"/>
            <TextInput source="password1" />
            <TextInput source="password2" />
        </SimpleForm>
    </Create>
);