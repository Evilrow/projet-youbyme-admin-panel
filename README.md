# Get Started

-> Cloner le projet. <br>
-> Se rendre à la racine du projet <br>
-> `yarn` pour installer les node_modules <br>

## Configuration

-> A la racine du projet se trouve deux fichiers : **dataprovider.js** & **authprovider.js**. Veuillez à bien changer les urls si Django n'est pas lancé sur le port 8000.
-> Sachant que l'api et l'application tourne en localhost, il faut télécharger l'extension  [Allow-Control-Allow-Origin](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi) (ici chrome). Si des problèmes subsitent, il suffit de désactiver et réactiver ce plugin. <br>

## Run in Dev (show all console errors)

-> `yarn start` pour lancer l'application <br>

## Run in Prod (No console errors)

-> `yarn build` pour builder l'application (le build dure une petite minute) <br>
-> Installer serve si besoin avec `yarn global add serve` puis démarrer via `serve -s build` <br>
